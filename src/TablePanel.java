import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.JPanel;

public class TablePanel extends JPanel {

	private int noRows = 4;
	private RowPanel[] allRows = new RowPanel[noRows];
	private double totalValoare;

	public TablePanel(ArrayList<String> values) {
		setLayout(new GridLayout(0, 1, 5, 5));

		for (int i = 0; i < noRows; i++) {
			RowPanel row = new RowPanel(values, i + 1);
			add(row);
			allRows[i] = row;
		}
	}

	public String getTotalValoare() {
		if (totalValoare > 0)
			return "" + totalValoare;
		else
			return "";
	}

	public ArrayList<String> getValues() {
		ArrayList<String> newValues = new ArrayList<String>();

		for (int i = 0; i < noRows; i++) {
			newValues.addAll(allRows[i].getValues());
			double valoareOperatieCurrenta = allRows[i].getValoareOperatie();
			totalValoare += valoareOperatieCurrenta;
			if (valoareOperatieCurrenta > 0)
				newValues.add("" + valoareOperatieCurrenta);
			else
				newValues.add("");
		}
		if (totalValoare > 0)
			newValues.add("" + totalValoare);
		else
			newValues.add("");

		return newValues;
	}

	public void resetValues() {

		for (int i = 0; i < noRows; i++) {
			allRows[i].resetValues();
		}
	}
}
