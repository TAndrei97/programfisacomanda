import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ImageCustom {

	private ArrayList<String> stringValues;
	private BufferedImage image;
	private boolean backIsPressed;
	private BufferedImage bimage;
	private String configFile;
	private String outputPath;
	private String inputPath;
	private int counterID;

	public ImageCustom(String aConfigFile, ArrayList<String> aStringValues) throws IOException {
		stringValues = aStringValues;
		configFile = aConfigFile;
		image = null;
		backIsPressed = false;
		bimage = null;

		getPaths();
		read();
		placeValues();
		scale();
	}

	public void read() throws IOException {
		image = ImageIO.read(new File(inputPath));
	}

	public void scale() {
		Image img = image.getScaledInstance(450, 600, Image.SCALE_FAST);
		bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

		Graphics2D bGr = bimage.createGraphics();
		bGr.drawImage(img, 0, 0, null);
		bGr.dispose();
	}

	private String getOutputFileString() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
		String currentDate = dateFormat.format(date);

		return outputPath + "\\" + currentDate + "-" + Integer.toString(counterID) + "-" + stringValues.get(1) + ".png";
	}

	public void save() throws IOException {
		Image imgToSave = image.getScaledInstance(892, 1263, Image.SCALE_FAST);

		BufferedImage bufferdImageToSave = new BufferedImage(imgToSave.getWidth(null), imgToSave.getHeight(null),
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D bGrToSave = bufferdImageToSave.createGraphics();
		bGrToSave.drawImage(imgToSave, 0, 0, null);
		bGrToSave.dispose();
		ImageIO.write(bufferdImageToSave, "PNG", new File(getOutputFileString()));
	}

	public BufferedImage get_image() {
		return image;
	}

	public void placeValues() {
		Graphics g = image.getGraphics();
		Font font = new Font("Arial", Font.PLAIN, 55);
		g.setFont(font);
		g.setColor(Color.BLACK);

		g.drawString(stringValues.get(0).toUpperCase(), 401, 489); // Marca
		g.drawString(stringValues.get(1).toUpperCase(), 308, 566); // Client
		g.drawString(stringValues.get(2).toUpperCase(), 1842, 489); // Serie
																	// caroserie
		g.drawString(stringValues.get(3).toUpperCase(), 1771, 566); // Serie
																	// motor
		g.drawString(stringValues.get(4).toUpperCase(), 210, 765); // lucrare 1
		g.drawString(stringValues.get(5).toUpperCase(), 210, 825); // lucrare 2
		g.drawString(stringValues.get(6).toUpperCase(), 990, 1150); // Termen
																	// executie
		g.drawString(stringValues.get(7).toUpperCase(), 630, 1214); // Cost
																	// estimativ
		g.drawString(stringValues.get(8).toUpperCase(), 1498, 1342); // Inventar
																		// auto
		g.drawString(stringValues.get(9).toUpperCase(), 416, 1396); // KM bord
		g.drawString(stringValues.get(10).toUpperCase(), 1076, 1462); // Cantitate
																		// combustibil

		// primul tabel
		for (int i = 0; i < 4; i++) {
			g.drawString(stringValues.get(11 + i * 4 + 0).toUpperCase(), 282, 1716 + i * 60);
			g.drawString(stringValues.get(11 + i * 4 + 1).toUpperCase(), 1820, 1716 + i * 60);
			g.drawString(stringValues.get(11 + i * 4 + 2).toUpperCase(), 1993, 1716 + i * 60);
			g.drawString(stringValues.get(11 + i * 4 + 3).toUpperCase(), 2150, 1716 + i * 60);
		}
		g.drawString(stringValues.get(27).toUpperCase(), 2150, 1956); // total
																		// tabel
																		// 1

		// al doilea table
		for (int i = 0; i < 4; i++) {
			g.drawString(stringValues.get(28 + i * 4 + 0).toUpperCase(), 282, 2210 + i * 60);
			g.drawString(stringValues.get(28 + i * 4 + 1).toUpperCase(), 1820, 2210 + i * 60);
			g.drawString(stringValues.get(28 + i * 4 + 2).toUpperCase(), 1993, 2210 + i * 60);
			g.drawString(stringValues.get(28 + i * 4 + 3).toUpperCase(), 2150, 2210 + i * 60);
		}
		g.drawString(stringValues.get(44).toUpperCase(), 2150, 2450); // total
																		// tabel
																		// 2

		g.drawString(stringValues.get(45).toUpperCase(), 2000, 2576); // total
																		// manopera
		g.drawString(stringValues.get(46).toUpperCase(), 2000, 2640); // total
																		// material
		g.drawString(stringValues.get(47).toUpperCase(), 1652, 402); // data
		g.drawString(Integer.toString(counterID), 710, 402); // Nr deviz

		g.dispose();
	}

	public boolean get_backIsPressed() {
		return backIsPressed;
	}

	public void display() throws IOException {

		JFrame frame = new JFrame("Fisa Comanda");
		JPanel panel = new JPanel(new BorderLayout(10, 10));
		ImageIcon imageToDisplay = new ImageIcon(bimage);
		JLabel label = new JLabel(imageToDisplay);
		JButton printButton = new JButton("Print");
		JButton backButton = new JButton("Back");
		JButton buttonPressed = new JButton();

		backButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				synchronized (buttonPressed) {
					frame.setVisible(false);
					frame.dispose();
					backIsPressed = true;
					buttonPressed.notify();
				}
			}
		});

		printButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				synchronized (buttonPressed) {
					try {
						save();
						updateCounter();
					} catch (IOException e) {
					}
					new Thread(new PrintActionListener(image)).start();

					frame.setVisible(false);
					frame.dispose();
					buttonPressed.notify();
				}
			}
		});
		JPanel controls = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 2));
		frame.setSize(bimage.getWidth(), bimage.getHeight() + 90);
		panel.add(label);
		controls.add(backButton);
		controls.add(printButton);
		panel.add(controls, BorderLayout.SOUTH);
		frame.add(panel);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		synchronized (buttonPressed) {
			try {
				buttonPressed.wait();
			} catch (InterruptedException e1) {
			}
		}
	}

	public void updateCounter() throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(configFile));
		counterID++;
		bw.write(Integer.toString(counterID));
		bw.write(System.getProperty("line.separator"));
		bw.write(inputPath);
		bw.write(System.getProperty("line.separator"));
		bw.write(outputPath);

		bw.close();
	}

	private void getPaths() throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new FileReader(configFile));

		counterID = Integer.parseInt(br.readLine());
		inputPath = br.readLine();
		outputPath = br.readLine();
		br.close();
	}

}
