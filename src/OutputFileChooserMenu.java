import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

public class OutputFileChooserMenu extends JFrame {

	public File outputFile;

	public OutputFileChooserMenu(String pathName) {
		// TODO Auto-generated constructor stub
		JFileChooser fileChooser = new JFileChooser();

		fileChooser.setSelectedFile(new File("newFisa.txt"));
		fileChooser.setCurrentDirectory(new File(pathName));
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

		if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
			outputFile = fileChooser.getCurrentDirectory();
		}
		add(fileChooser);
	}
}
