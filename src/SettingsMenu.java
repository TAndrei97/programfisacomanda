import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SettingsMenu extends JFrame {

	private String configFile;
	private String inputFilePath;
	private String outputFilePath;
	private int counterID;

	public SettingsMenu(String aConfigFile) throws NumberFormatException, IOException {

		configFile = aConfigFile;

		BufferedReader br = new BufferedReader(new FileReader(configFile));

		counterID = Integer.parseInt(br.readLine());
		inputFilePath = br.readLine();
		outputFilePath = br.readLine();
		br.close();

		JPanel panel = new JPanel(new BorderLayout(10, 10));

		JPanel controlsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 2));
		JPanel valuePanel = new JPanel(new BorderLayout(5, 5));

		JPanel inputPanel = new JPanel(new FlowLayout());
		JPanel outputPanel = new JPanel(new FlowLayout());
		JPanel counterPanel = new JPanel(new FlowLayout());

		JLabel inputLabel = new JLabel("Input file : ");
		JLabel outputLabel = new JLabel("Output file : ");
		JLabel counterLabel = new JLabel("Current ID : ");

		JTextField inputTextField = new JTextField(30);
		JTextField outputTextField = new JTextField(29);
		JTextField counterTextField = new JTextField(5);

		JButton inputButton = new JButton("...");
		JButton outputButton = new JButton("...");

		inputTextField.setText(inputFilePath);
		outputTextField.setText(outputFilePath);
		counterTextField.setText(Integer.toString(counterID));
		counterTextField.setHorizontalAlignment(JTextField.CENTER);

		inputTextField.setEditable(false);
		outputTextField.setEditable(false);

		inputButton.setPreferredSize(new Dimension(20, 20));
		outputButton.setPreferredSize(new Dimension(20, 20));

		inputPanel.add(inputLabel, FlowLayout.LEFT);
		inputPanel.add(inputTextField, FlowLayout.CENTER);
		inputPanel.add(inputButton, FlowLayout.RIGHT);

		outputPanel.add(outputLabel, FlowLayout.LEFT);
		outputPanel.add(outputTextField, FlowLayout.CENTER);
		outputPanel.add(outputButton, FlowLayout.RIGHT);

		counterPanel.add(counterLabel);
		counterPanel.add(counterTextField);

		valuePanel.add(inputPanel, BorderLayout.NORTH);
		valuePanel.add(outputPanel, BorderLayout.CENTER);
		valuePanel.add(counterPanel, BorderLayout.SOUTH);

		JButton closeButton = new JButton("Close");
		JButton saveButton = new JButton("Save");

		controlsPanel.add(closeButton);
		controlsPanel.add(saveButton);

		panel.add(valuePanel, BorderLayout.NORTH);
		panel.add(controlsPanel, BorderLayout.SOUTH);

		add(panel);
		setSize(600, 200);
		setVisible(true);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		closeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				setVisible(false);
				dispose();
			}
		});
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				counterID = Integer.parseInt(counterTextField.getText());
				setVisible(false);
				dispose();
				try {
					saveNewConfig();
				} catch (IOException e1) {
				}
			}

		});
		inputButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InputFileChooserMenu chooser = new InputFileChooserMenu(inputFilePath);
				if (chooser.inputFile != null) {
					inputFilePath = chooser.inputFile.toString();
					inputTextField.setText(inputFilePath);
				}
			}
		});
		outputButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// synchronized (resetButton) {

				OutputFileChooserMenu chooser = new OutputFileChooserMenu(outputFilePath);
				if (chooser.outputFile != null) {
					outputFilePath = chooser.outputFile.toString();
					outputTextField.setText(outputFilePath);
				}
			}
		});
	}

	private void saveNewConfig() throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(configFile));

		bw.write(Integer.toString(counterID));
		bw.write(System.getProperty("line.separator"));
		bw.write(inputFilePath);
		bw.write(System.getProperty("line.separator"));
		bw.write(outputFilePath);

		bw.close();
	}
}
