import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

public class ValueMenu {

	private JButton testButton;

	private ArrayList<String> stringValues;
	private boolean okIsPressed;
	public void setOkIsPressed(boolean okIsPressed) {
		this.okIsPressed = okIsPressed;
	}

	public void setPreviewIsPressed(boolean previewIsPressed) {
	}

	private String[] labelString = { "Model", "Client", "Serie caroserie", "Serie motor", "Lucrare 1", "Lucrare 2",
			"Termen de executie", "Cost estimativ", "Inventar auto", "KM bord", "Cantitate combustibil" };

	public String[] firstTableArray = { "Denumire operatie", "Timp", "Tarif" };
	public String[] secondTableArray = { "Denumire piesa", "U/M", "Pret unitar" };

	private ArrayList<String> firstTableValues = new ArrayList<String>(Arrays.asList(firstTableArray));
	private ArrayList<String> secondTableValues = new ArrayList<String>(Arrays.asList(secondTableArray));

	private String[] carModels = { "Alfa Romeo", "Audi", "BMW", "Cadillac", "Chevrolet", "Chrysler", "Citroen", "Dacia",
			"Daewoo", "Daihatsu", "Dodge", "Fiat", "Ford", "Honda", "Hyundai", "Infiniti", "Iveco","Jaguar", "Jeep", "Kia",
			"Land Rover", "Lexus", "Mazda", "Mercedes-Benz", "Mini", "Mitsubishi", "Nissan", "Opel", "Peugeot", "Rover",
			"Renault", "Saab", "Seat", "Skoda", "Smart", "Subaru", "Suzuki", "Toyota", "Volkswagen", "Volvo" };

	private int fieldNumbers = labelString.length;
	private JFrame valueFrame;

	private String configFile;

	public ValueMenu(String configFile) {
		this.configFile = configFile;
		okIsPressed = false;
		stringValues = new ArrayList<String>();
		for (int i = 0; i < fieldNumbers; i++) {
			stringValues.add("");
		}
	}

	public void printValuesMenu() {
		valueFrame = new JFrame("Fisa Comanda");

		JTextField[] txtAllAverages;

		float fontSize = 20.0f;

		txtAllAverages = new JTextField[fieldNumbers + 2];

		JPanel inputControls = new JPanel(new BorderLayout(5, 5));

		JPanel inputControlsLabels = new JPanel(new GridLayout(0, 1, 3, 3));
		JPanel inputControlsFields = new JPanel(new GridLayout(0, 1, 3, 3));
		inputControls.add(inputControlsLabels, BorderLayout.WEST);
		inputControls.add(inputControlsFields, BorderLayout.CENTER);

		JComboBox<String> comboBox = new JComboBox<>();
		for (int i = 0; i < carModels.length; i++) {
			comboBox.addItem(carModels[i]);
		}

		JLabel auxLabel = new JLabel(labelString[0]);
		auxLabel.setFont(auxLabel.getFont().deriveFont(fontSize));
		inputControlsLabels.add(auxLabel);
		inputControlsFields.add(comboBox);

		for (int i = 1; i < fieldNumbers; i++) {

			auxLabel = new JLabel(labelString[i]);
			auxLabel.setFont(auxLabel.getFont().deriveFont(fontSize));
			inputControlsLabels.add(auxLabel);
			JTextField field = new JTextField(10);
			field.setFont(field.getFont().deriveFont(fontSize));
			inputControlsFields.add(field);
			txtAllAverages[i] = field;
			txtAllAverages[i].setText(stringValues.get(i));
		}
		TablePanel firstTablePanel = new TablePanel(firstTableValues);
		TablePanel secondTablePanel = new TablePanel(secondTableValues);

		JPanel tabelPanel = new JPanel(new GridLayout(0, 1, 3, 3));
		tabelPanel.add(firstTablePanel);
		tabelPanel.add(secondTablePanel);

		inputControls.add(tabelPanel, BorderLayout.SOUTH);

		JPanel controls = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 2));

		JButton okButton = new JButton("Print");
		JButton resetButton = new JButton("Reset");
		JButton settingsButton = new JButton("Settings");
		JButton previewButton = new JButton("Preview");
		testButton = new JButton("test");

		controls.add(settingsButton);
		controls.add(resetButton);
		controls.add(previewButton);
		controls.add(okButton);

		JPanel gui = new JPanel(new BorderLayout(10, 10));
		gui.setBorder(new TitledBorder("Valori"));
		gui.add(inputControls, BorderLayout.CENTER);
		gui.add(controls, BorderLayout.SOUTH);

		settingsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					SettingsMenu settingsMenu = new SettingsMenu(configFile);
				} catch (NumberFormatException | IOException e1) {
				}
			}
		});

		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				comboBox.setSelectedIndex(0);
				firstTablePanel.resetValues();
				secondTablePanel.resetValues();
				for (int i = 1; i < fieldNumbers; i++) {
					txtAllAverages[i].setText("");
				}
			}
		});

		previewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				synchronized (testButton) {
					stringValues.clear();
					stringValues.add(carModels[comboBox.getSelectedIndex()]);
					for (int i = 1; i < fieldNumbers; i++) {
						stringValues.add(txtAllAverages[i].getText());
					}
					stringValues.addAll(firstTablePanel.getValues());
					stringValues.addAll(secondTablePanel.getValues());
					stringValues.add(firstTablePanel.getTotalValoare());
					stringValues.add(secondTablePanel.getTotalValoare());
					testButton.notify();
				}
			}
		});

		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				synchronized (testButton) {
					stringValues.clear();
					stringValues.add(carModels[comboBox.getSelectedIndex()]);
					for (int i = 1; i < fieldNumbers; i++) {
						stringValues.add(txtAllAverages[i].getText());
					}
					stringValues.addAll(firstTablePanel.getValues());
					stringValues.addAll(secondTablePanel.getValues());
					stringValues.add(firstTablePanel.getTotalValoare());
					stringValues.add(secondTablePanel.getTotalValoare());
					okIsPressed = true;
					testButton.notify();
				}
			}
		});

		JScrollPane scr = new JScrollPane(gui);
		scr.getVerticalScrollBar().setUnitIncrement(16);
		scr.setPreferredSize(new Dimension(800, 500));
		valueFrame.setContentPane(scr);
		valueFrame.pack();
		valueFrame.setLocationByPlatform(true);
		valueFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		valueFrame.setLocationRelativeTo(null);
		valueFrame.setVisible(true);

		synchronized (testButton) {
			try {
				testButton.wait();
				return;
			} catch (InterruptedException e1) {
			}
		}
	}

	public void dispose() {
		valueFrame.dispose();
	}

	public void hideMenu() {
		valueFrame.setVisible(false);
	}

	public void showMenu() {
		valueFrame.setVisible(true);
		synchronized (testButton) {
			try {
				testButton.wait();
				return;
			} catch (InterruptedException e1) {
			}
		}
	}

	public ArrayList<String> getValues() {
		return stringValues;
	}

	public void setValues(ArrayList<String> values) {
		this.stringValues = values;
	}

	public boolean okIsPressed() {
		return okIsPressed;
	}
}
