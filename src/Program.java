import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Program {

	public static void main(String[] args) throws IOException {
		String configFile = "configFC.txt";
		ArrayList<String> valueStrings = new ArrayList<String>();
		ValueMenu menu = new ValueMenu(configFile);
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		Date date = new Date();
		String currentDate = dateFormat.format(date);

		menu.printValuesMenu();

		while (true) {
			menu.hideMenu();
			valueStrings = menu.getValues();
			valueStrings.add(currentDate);

			ImageCustom image = new ImageCustom(configFile, valueStrings);

			if (!menu.okIsPressed()) {
				image.display();
			} else {
				try {
					image.save();
					image.updateCounter();
				} catch (IOException e) {
				}
				BufferedImage inputImage = image.get_image();
				new Thread(new PrintActionListener(inputImage)).start();
			}

			menu.setOkIsPressed(false);
			menu.setPreviewIsPressed(false);
			menu.showMenu();
		}
	}
}
