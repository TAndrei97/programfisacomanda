import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class RowPanel extends JPanel {
	private int noCol;
	private JTextField textField;
	private ArrayList<String> values;
	private final float fontSize = 20.0f;

	private JTextField[] allTextFields;

	public RowPanel(ArrayList<String> values, int rowNo) {

		this.values = values;
		noCol = values.size();
		JLabel text = new JLabel("" + rowNo);
		allTextFields = new JTextField[noCol];

		text.setFont(text.getFont().deriveFont(fontSize));
		add(text);
		textField = new JTextField(15);
		add(textField);
		textField.setFont(textField.getFont().deriveFont(fontSize));
		allTextFields[0] = textField;
		allTextFields[0].setText(values.get(0));

		allTextFields[0].addFocusListener(new FocusListener() {

			@Override
			public void focusGained(FocusEvent e) {
				if (allTextFields[0].getText().equals(values.get(0)))
					allTextFields[0].setText(null); // Empty the text field when
													// it receives focus
			}

			@Override
			public void focusLost(FocusEvent e) {
				// You could do something here when the field loses focus, if
				// you like
				if (allTextFields[0].getText().length() == 0)
					allTextFields[0].setText(values.get(0));
			}

		});

		for (int i = 1; i < values.size(); i++) {
			final String oldValue = values.get(i);
			textField = new JTextField(6);
			add(textField);
			textField.setFont(textField.getFont().deriveFont(fontSize));
			allTextFields[i] = textField;
			allTextFields[i].setText(oldValue);

			JTextField auxTF = allTextFields[i];

			auxTF.addFocusListener(new FocusListener() {

				@Override
				public void focusGained(FocusEvent e) {
					if (auxTF.getText().equals(oldValue))
						auxTF.setText(null);
				}

				@Override
				public void focusLost(FocusEvent e) {
					if (auxTF.getText().length() == 0)
						auxTF.setText(oldValue);
				}
			});
		}
	}

	public void resetValues() {
		for (int i = 0; i < values.size(); i++) {
			allTextFields[i].setText(values.get(i));
		}
	}

	public double getValoareOperatie() {

		double pretBuc = getPretBuc();
		double nrBuc = getNrBuc();
		double total = pretBuc * nrBuc;
		return Math.floor(total * 100) / 100;

	}

	public double getPretBuc() {
		String pretStr = allTextFields[2].getText();
		double pretDouble = 0;
		try {
			pretStr = pretStr.replace(",", ".");
			pretDouble = Double.parseDouble(pretStr);
		} finally {
			return Math.floor(pretDouble * 100) / 100;
		}
	}

	public double getNrBuc() {
		String pretStr = allTextFields[1].getText();
		double pretDouble = 0;
		try {
			pretStr = pretStr.replace(",", ".");
			pretDouble = Double.parseDouble(pretStr);
		} finally {
			return Math.floor(pretDouble * 100) / 100;
		}
	}

	public ArrayList<String> getValues() {
		ArrayList<String> newValues = new ArrayList<String>();
		for (int i = 0; i < noCol; i++) {
			if (allTextFields[i].getText().equals(values.get(i)))
				newValues.add("");
			else
				newValues.add(allTextFields[i].getText());
		}
		return newValues;
	}
}
