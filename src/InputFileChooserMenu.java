import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

public class InputFileChooserMenu extends JFrame {

	public File inputFile;

	public InputFileChooserMenu(String pathName) {

		JFileChooser fileChooser = new JFileChooser();

		fileChooser.setCurrentDirectory(new File(pathName));
		fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Image files", ImageIO.getReaderFileSuffixes()));
		fileChooser.setAcceptAllFileFilterUsed(false);

		if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			inputFile = fileChooser.getSelectedFile();
		}

		add(fileChooser);
	}
}
